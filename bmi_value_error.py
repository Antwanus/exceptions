height = float(input("Height: "))
weight = int(input("Weight: "))

if height > 3:
    raise ValueError(f"Height cannot be <{height}> meters")

bmi = weight / height**2
print(bmi)

