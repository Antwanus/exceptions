posts = [
    {'Likes': 21, 'Comments': 2},
    {'Likes': 13, 'Comments': 2, 'Shares': 1},
    {'Likes': 33, 'Comments': 8, 'Shares': 3},
    {'Comments': 4, 'Shares': 2},
    {'Comments': 1, 'Shares': 1},
    {'Likes': 19, 'Comments': 3},
]

total_likes = 0

for post in posts:
    post_likes = 0
    try:
        post_likes = post['Likes']
    except KeyError:
        pass
    finally:
        total_likes += post_likes

print(total_likes)

