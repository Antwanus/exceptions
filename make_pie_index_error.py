fruits = ["Apple", "Pear", "Orange"]


def make_pie(index):
    try:
        fruit = fruits[index]
    except IndexError as e:
        print('Index Error:', e)
        print("Fruit Pie!")
    else:
        print(f"{fruit} pie!")


make_pie(4)
