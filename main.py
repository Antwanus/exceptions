try:
    file = open('a_file.txt')
    a_dict = {'some_key': 'some_value'}
    # print(a_dict["abc"])
except FileNotFoundError as file_not_found_e:
    print('An error occurred:', file_not_found_e)
    print('Creating a new file \'a_file.txt\'')
    file = open("a_file.txt", "w")
    file.write("something.. something.. dark side.")
except KeyError as key_e:
    print('A key has not been found in the dictionary:', key_e)
else:
    content = file.read()
    print('Content of the file:\n', content)
finally:
    print('bye')
    raise NotImplementedError("raising this error in finally")
